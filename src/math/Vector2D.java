package math;

import java.util.Objects;

public class Vector2D implements Vector<Vector2D> {

    private double x;
    private double y;

    public Vector2D() {
        this.x = 0;
        this.y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Vector2D zero(){
        return new Vector2D(0,0);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public Vector2D sum(Vector2D vector) {
        this.x = this.x+vector.getX();
        this.y = this.y+vector.getY();
        return this;
    }

    @Override
    public Vector2D sub(Vector2D vector) {
        this.x = this.x-vector.getX();
        this.y = this.y-vector.getY();
        return this;
    }

    @Override
    public Vector2D mul(double scalar) {
        this.x = this.x*scalar;
        this.y = this.y*scalar;
        return this;
    }

    @Override
    public Vector2D div(double div) {
        this.x = this.x/div;
        this.y = this.y/div;
        return this;
    }

    @Override
    public String toString() {
        return "math.Vector2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2D vector2D = (Vector2D) o;
        return Double.compare(vector2D.x, x) == 0 &&
                Double.compare(vector2D.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public Vector2D copy(){
        return new Vector2D(this.x, this.y);
    }
}
