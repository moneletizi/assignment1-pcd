package math;

public interface Vector<V extends Vector> {

    /*alternativa poteva essere quella di usare un approccio più functional con funzioni
            che prendevano come parametri due vector e ritornavano un vector, risultato dell'operazione */

    V sum(V vector);
    V sub(V vector);
    V mul(double scalar);
    V div(double div);

}
