package simulation;

import java.util.List;

public interface Simulation {
    void startSimulation();
    void stopSimulation();
    void waitSimulation();
}
