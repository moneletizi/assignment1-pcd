package simulation;

import model.MultiParticleSpace;
import model.interfaces.Particle;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelSimulation extends AbstractSimulation {

    private List<Worker> mWorkers;
    private int avaibleProcessor;

    private int numberOfWorker;
    private int particlesForWorker;
    private int particlesExtra;

    public ParallelSimulation(MultiParticleSpace multiParticleSpace, int n_step, double dt) {
        super(multiParticleSpace, n_step, dt);
        this.avaibleProcessor = Runtime.getRuntime().availableProcessors();
        this.numberOfWorker = avaibleProcessor+1;
        this.particlesForWorker = mParticleSpace.getParticles().size()/numberOfWorker;
        this.particlesExtra = mParticleSpace.getParticles().size()%numberOfWorker;
    }

    @Override
    public void simulate() {
        setRunning(true);
        mWorkers = IntStream.range(0,numberOfWorker).boxed()
                .map(i->(i!=numberOfWorker)? new Worker(particlesForWorker, (i)*particlesForWorker): new Worker(particlesForWorker, (i)*particlesForWorker+particlesExtra))
                .collect(Collectors.toList());
        mWorkers.forEach(Worker::start);


        for(int i=currentStep ;i<n_step && isRunning() ;i++){
            mWorkers.stream().forEach(Worker::startWork);
            mWorkers.stream().forEach(Worker::waitTermination);
            mParticleSpace.setParticles(mWorkers.stream().map(x -> x.getParticlesAtDtTime()).reduce((x,y) -> {
                x.addAll(y);
                return x;
            }).get());

            currentStep = i;
        }
        setRunning(false);

        mWorkers.forEach(Thread::interrupt);
        mWorkers.forEach(w->{
            try{
                w.join();
            }catch (InterruptedException exc){
                exc.printStackTrace();
            }
        });


    }

    private class Worker extends Thread {

        private List<Particle> particlesAtDtTime = new ArrayList<>();
        private Semaphore initCompute;
        private Semaphore notifyComputed;
        private int nParticles;
        private int from;
        private boolean isActive = true;

        public Worker (int nParticles, int from){
            this.nParticles = nParticles;
            this.from = from;
            initCompute = new Semaphore(0);
            notifyComputed = new Semaphore(0);
        }

        @Override
        public void run() {
            while (isActive){
                try {
                //prima di riniziare a calcolare devo attendere che tutti abbiano aggiornato la lista, sarà il  a sbloccarmi solo
                //dopo che avrà la certezza che tutti hanno finito
                    initCompute.acquire();
                    particlesAtDtTime.clear();//pulisco la lista
                    mParticleSpace.getParticles().stream().skip(from).limit(nParticles).forEach(p -> {
                        Particle pCopy = p.copy();
                        pCopy.computeAndApplyForce(mParticleSpace.getParticles(), mParticleSpace.getK(), d_time, mParticleSpace.getkAttr());
                        particlesAtDtTime.add(pCopy);
                    });
                    notifyComputed.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    isActive = false;
                }
            }
        }

        public void startWork() {
            initCompute.release();
        }

        public void waitTermination(){
            try {
                notifyComputed.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public List<Particle> getParticlesAtDtTime() {
            return particlesAtDtTime;
        }

    }
}
