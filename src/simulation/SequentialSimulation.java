package simulation;

import model.MultiParticleSpace;
import model.interfaces.Particle;

import java.util.ArrayList;
import java.util.List;

public class SequentialSimulation extends AbstractSimulation {


    public SequentialSimulation(MultiParticleSpace multiParticleSpace, int n_step, double dt) {
        super(multiParticleSpace, n_step, dt);
    }

    @Override
    public void simulate() {
        for(int i=0;i<n_step && isRunning();i++){
            List<Particle> particlesAtDtTime = new ArrayList<>();
            setRunning(true);
            for(Particle p: mParticleSpace.getParticles()) {
                Particle particleCopy = p.copy();
                particleCopy.computeAndApplyForce(mParticleSpace.getParticles(), mParticleSpace.getK(), d_time, mParticleSpace.getkAttr());
                particlesAtDtTime.add(particleCopy);
            }
            mParticleSpace.setParticles(particlesAtDtTime);
        }
        setRunning(false);

    }
}
