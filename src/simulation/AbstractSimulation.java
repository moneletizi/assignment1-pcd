package simulation;

import model.MultiParticleSpace;

public abstract class AbstractSimulation implements Simulation {

    protected final int n_step;
    protected final double d_time;
    protected MultiParticleSpace mParticleSpace;
    private Thread activeObjectThread;
    protected int currentStep;
    protected Flag mFlag;

    public AbstractSimulation(MultiParticleSpace multiParticleSpace, final int n_step, final double dt){
        this.mParticleSpace = multiParticleSpace;
        this.n_step = n_step;
        this.d_time = dt;
        this.currentStep = 0;
        this.mFlag = new Flag(true);
    }

    protected abstract void simulate();

    public void startSimulation(){
        activeObjectThread = new Thread(()->simulate());
        activeObjectThread.start();
    }

    protected void setRunning(boolean running) {
        mFlag.setFlag(running);
         /*questi metodi sono syncronized (all'interno di un monitor Flag)
            perchè questo metodo viene richiamato dall'event dispatcher thread mentre il thread di simulation
            effettua un accesso in lettura nell'if per continuare
        */
    }

    protected boolean isRunning(){
        return mFlag.getFlag();
    }

    public void stopSimulation(){
        setRunning(false);
    }

    public void waitSimulation(){
        try {
            activeObjectThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
