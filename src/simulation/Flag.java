package simulation;

public class Flag {

    //monitor per gestire il fatto che la simulazione sia attiva o meno

    private boolean flag;

    public Flag(boolean state){
       this.flag = state;
    }

    public synchronized void setFlag(boolean state){
        flag = state;
    }

    public synchronized boolean getFlag(){
        return flag;
    }

}
