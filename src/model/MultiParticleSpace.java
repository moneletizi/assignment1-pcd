package model;

import com.sun.xml.internal.bind.v2.util.CollisionCheckStack;
import model.interfaces.Observable;
import model.interfaces.Particle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MultiParticleSpace implements Observable {

    public interface Observer {
        void update();
    }

    private List<Observer> mObserversList = new ArrayList<>();
    private List<Particle> particles;
    private final double k;
    private final double kAttr;

    public MultiParticleSpace(List<Particle> particles, final double k, final double kAttr) {
        this.particles = particles;
        this.k = k;
        this.kAttr = kAttr;
    }

    public void setParticles(List<Particle> particles) {
        this.particles.clear();
        this.particles.addAll(particles);
        notifyAllObserver();
    }

    public double getK() {
        return k;
    }

    public double getkAttr() {
        return kAttr;
    }

    /*public void simulate(){

        for(int i=0;i<n_step;i++){
            List<Particle> particlesAtDtTime = new ArrayList<>();
            //prima di riniziare a calcolare devo attendere che tutti abbiano aggiornato la lista

            for(Particle p: particles) {
                Particle particleCopy = p.copy();
                particleCopy.computeAndApplyForce(particles, k, dt);
                particlesAtDtTime.add(particleCopy);
            }

            //prima di fare questo assegnamento tutti devono aver finito di calcolare la loro sottoporzione altrimenti andrei a scrivere
            //su particles mentre qualc'un altro ci sta leggendo
            setParticles(particlesAtDtTime);
            //particles = particlesAtDtTime;
            //dico che io ho scritto e ho aggiorato la lista; dopo aver aggionrato la lista potrei terminare prima di qualcun'altro
            //questo mi fa ritornare su ma devo bloccarmi

        }
    }*/



    public List<Particle> getParticles(){
        return new ArrayList<>(particles);
    }


    public void attachObserver(Observer observer){
        mObserversList.add(observer);
    }

    private void notifyAllObserver(){
        mObserversList.forEach(o->o.update());
    }

}
