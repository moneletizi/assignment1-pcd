package model;

import math.Vector2D;
import model.interfaces.Particle;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Objects;

public class ParticleImpl implements Particle {

    private static final  double MIN_VALUE = 0.000001d;

    private  int id;
    private double alpha; //carica
    private Point2D relativePos;
    private double m;
    private Vector2D velocity;

    public ParticleImpl(int id, double alpha, Point2D relativePos, double m, Vector2D velocity) {
        this.id = id;
        this.alpha = alpha;
        this.relativePos = relativePos;
        this.m = m;
        this.velocity = velocity;
    }

    @Override
    public double getAlpha() {
        return alpha;
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    @Override
    public Point2D getRelativePos() {
        return relativePos;
    }

    @Override
    public void setRelativePos(Point2D relativePos) {
        this.relativePos = relativePos;
    }

    @Override
    public double getM() {
        return m;
    }

    @Override
    public void setM(int m) {
        this.m = m;
    }

    @Override
    public Vector2D getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticleImpl particle = (ParticleImpl) o;
        return id == particle.id &&
                Double.compare(particle.alpha, alpha) == 0 &&
                Double.compare(particle.m, m) == 0 &&
                Objects.equals(relativePos, particle.relativePos) &&
                Objects.equals(velocity, particle.velocity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, alpha, relativePos, m, velocity);
    }

    private double getDistance(Particle particle){
        double distance = relativePos.distance(particle.getRelativePos());
        return (distance==0d) ? MIN_VALUE :distance;
    }

    private Point2D getDirection(Particle p){
        double directionX = relativePos.getX()-p.getRelativePos().getX();
        double directionY = relativePos.getY()-p.getRelativePos().getY();
        if(directionX==0d && directionY==0d){
            return new Point2D.Double(MIN_VALUE, MIN_VALUE);
        }
        return new Point2D.Double( directionX, directionY);
    }

    private Vector2D computeForce(List<Particle> particles, final double k, final double k_attr){
        Vector2D f = new Vector2D();
        for(Particle p:particles){
            if(!p.equals(this)){
                double distance = getDistance(p);
                double d3 = distance*distance*distance;
                double factor = (getAlpha()*p.getAlpha()*k/d3);
                double fx = getDirection(p).getX()*factor;
                double fy = getDirection(p).getY()*factor;
                double fAttrx = -(k_attr)*getVelocity().getX();
                double fAttry = -(k_attr)*getVelocity().getY();

                Vector2D force = new Vector2D(fx+fAttrx,fy+fAttry);
                f.sum(force);
            }
        }
        return f;
    }

    private void applyForce(Vector2D force, double dt){
        double newX = getRelativePos().getX() + getVelocity().copy().mul(dt).getX();//r[i][x] += dt*v[i][x];
        double newY = getRelativePos().getY() + getVelocity().copy().mul(dt).getY(); //r[i][y] += dt*v[i][y];

        setRelativePos(new Point2D.Double(newX, newY));
        setVelocity(getVelocity().sum(force.mul(dt).div(m))); //v[i][x][y] += dt*F[i][x][y]/m[i];
    }

    @Override
    public void computeAndApplyForce(List<Particle> particles, final double k, final double dt, final double k_attr){
        applyForce(computeForce(particles, k, k_attr), dt);
    }


    @Override
    public ParticleImpl copy(){
        return new ParticleImpl(getId(), getAlpha(), (Point2D) getRelativePos().clone(),getM(), getVelocity().copy());
    }

    @Override
    public String toString() {
        return "ParticleImpl{" +
                "id=" + id +
                ", alpha=" + alpha +
                ", relativePos=" + relativePos +
                ", m=" + m +
                ", velocity=" + velocity +
                '}';
    }
}
