package model.interfaces;

import math.Vector2D;

import java.awt.geom.Point2D;
import java.util.List;

public interface Particle {

    double getAlpha();

    void setAlpha(int alpha);

    Point2D getRelativePos();

    void setRelativePos(Point2D relativePos);

    double getM();

    void setM(int m);

    Vector2D getVelocity();

    void setVelocity(Vector2D velocity);

    void computeAndApplyForce(List<Particle> particles, final double k, final double dt, final double k_attr);

    Particle copy();

}
