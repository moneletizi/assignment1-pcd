package model.interfaces;

import model.MultiParticleSpace;

import java.util.List;

public interface Observable {
    void attachObserver(MultiParticleSpace.Observer observer);
    List<Particle> getParticles();
}
