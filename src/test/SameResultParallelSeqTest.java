package test;

import math.Vector2D;
import model.MultiParticleSpace;
import simulation.ParallelSimulation;
import simulation.SequentialSimulation;
import model.ParticleImpl;
import model.interfaces.Particle;
import simulation.Simulation;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SameResultParallelSeqTest {

    public static void main(String... args) {
        final List<Particle> particles1 = IntStream.range(0, 10)
                .boxed()
                .map(i -> new ParticleImpl(i,1, new Point2D.Double(Math.random(), Math.random()),  1, Vector2D.zero()))
                .collect(Collectors.toList());

        final List<Particle> particles2 = particles1.stream().map(Particle::copy).collect(Collectors.toList());
        final MultiParticleSpace multiParticleSpace = new MultiParticleSpace(particles1, 0.1d, 0.6d);
        final MultiParticleSpace multiParticleSpace2 = new MultiParticleSpace(particles2, 0.1d, 0.6d);
        System.out.println("Initial result: ");
        System.out.println(multiParticleSpace.getParticles().equals(multiParticleSpace2.getParticles()));

        final Simulation sequential = new SequentialSimulation(multiParticleSpace, 2, 1);
        final Simulation parallel = new ParallelSimulation(multiParticleSpace2, 2, 1);

        sequential.startSimulation();
        sequential.waitSimulation();
        parallel.startSimulation();
        parallel.waitSimulation();

        System.out.println("Sequential result: ");
        System.out.println(multiParticleSpace.getParticles());

        System.out.println("Parallel result: ");
        System.out.println(multiParticleSpace2.getParticles());

        System.out.println("Same result: " + particles1.equals(particles2));


    }
}
