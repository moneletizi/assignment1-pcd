package test;

import math.Vector2D;
import simulation.AbstractSimulation;
import model.MultiParticleSpace;
import simulation.ParallelSimulation;
import model.ParticleImpl;
import model.interfaces.Particle;
import view.ViewerFrame;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Test {

    private static final int N_STEPS = 1000;
    private static final double K = 1;
    private static final double K_ATTR = 0.7d;
    private static final double dt = 0.0009d;
    private static final int N_PARTICLES = 333;

    public static void main(String[] args) {
        final List<Particle> particles1 = IntStream.range(0, N_PARTICLES)
                .boxed()
                .map(i -> new ParticleImpl(i, 1, new Point2D.Double(Math.random(), Math.random()),  Math.random(), Vector2D.zero()))
                .collect(Collectors.toList());

        MultiParticleSpace multiParticleSpace = new MultiParticleSpace(particles1, K, K_ATTR);
        AbstractSimulation simulation = new ParallelSimulation(multiParticleSpace, N_STEPS, dt);
        ViewerFrame frame = new ViewerFrame(simulation, multiParticleSpace,620,620);
        frame.setVisible(true);
    }


}
