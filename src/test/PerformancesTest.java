package test;

import math.Vector2D;
import model.MultiParticleSpace;
import simulation.ParallelSimulation;
import simulation.SequentialSimulation;
import model.ParticleImpl;
import model.interfaces.Particle;
import simulation.Simulation;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PerformancesTest {

    public static void main(String[] args) {
        final List<Particle> particles1 = IntStream.range(0, 1000)
                .boxed()
                .map(i -> new ParticleImpl(i,1,new Point2D.Double(Math.random(), Math.random()), 1, Vector2D.zero()))
                .collect(Collectors.toList());

        //final List<Particle> particles2 = particles1.stream().map(Particle::copy).collect(Collectors.toList());
        final MultiParticleSpace multiParticleSpace = new MultiParticleSpace(particles1, 0.1d, 0.6d);
        final Simulation sequential = new SequentialSimulation(multiParticleSpace, 1000, 1);
        final Simulation parallel = new ParallelSimulation(multiParticleSpace, 1000, 1);

        Chrono chrono = new Chrono();

        chrono.start();
        sequential.startSimulation();
        sequential.waitSimulation();
        chrono.stop();
        final long timeSequential = chrono.getTime();

        chrono.start();
        parallel.startSimulation();
        parallel.waitSimulation();
        chrono.stop();
        final long timeParallel = chrono.getTime();


        System.out.println("Performance result:");
        System.out.println("\tSequential: " + timeSequential + "[ms]");
        System.out.println("\tParallel: " + timeParallel + "[ms]");
        System.out.println("\tSpeedUp: " + (timeSequential / (double)timeParallel));
    }

    public static class Chrono {

        private boolean running;
        private long startTime;

        public Chrono(){
            running = false;
        }

        public void start(){
            running = true;
            startTime = System.currentTimeMillis();
        }

        public void stop(){
            startTime = getTime();
            running = false;
        }

        public long getTime(){
            if (running){
                return   System.currentTimeMillis() - startTime;
            } else {
                return startTime;
            }
        }
    }
}
