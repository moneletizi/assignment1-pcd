package view;

import com.sun.glass.ui.View;
import model.interfaces.Particle;
import view.mapper.MapUtils;

import java.awt.*;
import java.util.function.Function;

public class MassParticleDrawer implements ParticleDrawer {

    public static final int OUT_MAX_OVAL_DIAMETER = 20;

    ViewerFrame.ViewerMapper mViewerMapper;

    public MassParticleDrawer(ViewerFrame.ViewerMapper viewerMapper){
        this.mViewerMapper = viewerMapper;
    }

    @Override
    public void draw(Particle particle, Graphics2D g) {
        g.setBackground(Color.blue);
        g.fillOval(mViewerMapper.mapXCoordinate(particle.getRelativePos().getX()),
                mViewerMapper.mapYCoordinate(particle.getRelativePos().getY()),
                mapMassToDiameter(particle),
                mapMassToDiameter(particle));
    }

    private int mapMassToDiameter(Particle particle){
        return MapUtils.mapInRange(particle.getM(), OUT_MAX_OVAL_DIAMETER);
    }

}
