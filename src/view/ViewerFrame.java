package view;

import model.MultiParticleSpace;
import model.interfaces.Observable;
import model.interfaces.Particle;
import simulation.Simulation;
import view.mapper.MapUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewerFrame extends JFrame implements ActionListener, MultiParticleSpace.Observer {

    private int height;
    private int weight;

    private ParticlesSpacePanel panel;
    private JPanel buttonsPanel;
    private JButton buttonStart;
    private JButton buttonStop;

    private Observable mMultiParticleSpace;
    private Simulation simulation;

    public ViewerFrame(Simulation simulation, MultiParticleSpace multiParticleSpace, int w, int h){
        this.height = h;
        this.weight = w;

        this.simulation = simulation;
        this.mMultiParticleSpace = multiParticleSpace;
        this.mMultiParticleSpace.attachObserver(this);

        setTitle("Smart Position");
        setSize(w,h);
        setResizable(false);

        buttonStart = new JButton("start");
        buttonStop = new JButton("stop");
        buttonsPanel = new JPanel();
        buttonsPanel.add(buttonStart);
        buttonsPanel.add(buttonStop);
        buttonStart.addActionListener(this);
        buttonStop.addActionListener(this);

        panel = new ParticlesSpacePanel(w, h, new MassParticleDrawer(new ViewerMapper()));

        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(buttonsPanel);
        split.setBottomComponent(panel);
        split.setDividerLocation(0.8);

        getContentPane().add(split);

        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev){
                System.exit(-1);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if(src.equals(buttonStart)){
            this.simulation.startSimulation(); //attiva la simulazione che al suo interno utilizzerà un thread e avvierà tutto su un nuovo flusso di controllo
        }else if(src.equals(buttonStop)){
            this.simulation.stopSimulation();
        }
    }


    @Override
    public void update() {
        panel.updatePositions(mMultiParticleSpace.getParticles());
    }

    public class ViewerMapper{
        public int mapXCoordinate(double xPos){
            return MapUtils.mapInRange(xPos, weight/2);
        }

        public int mapYCoordinate(double yPos){
            return MapUtils.mapInRange(yPos, height/2);
        }

    }

    public static class ParticlesSpacePanel extends JPanel {

        private List<Particle> mParticlesToView;
        private ParticleDrawer mParticleDrawer;

        public ParticlesSpacePanel(int w, int h, ParticleDrawer particleDrawer){
            setSize(w,h);
            this.mParticleDrawer = particleDrawer;
        }

        public void paint(Graphics g){
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);
            g2.clearRect(0,0,this.getWidth(),this.getHeight());
            synchronized (this){
                if (mParticlesToView!=null){
                        mParticlesToView.forEach(p -> {
                            mParticleDrawer.draw(p, g2);
                    });
                }

            }

        }

        public void updatePositions(List<Particle> pos){
            /*  è necessario mettere syncronized perchè la lista mParticlesToView è utilizzata da due
                flussi differenti  (il thread che gestisce simulate e l'event dispatcher thread,
                richiamato attraverso SwingUtilities.invokeLater a gestire il repaint.
                Questo fa si che se il model è più veloce a produrre dei risultati essendo all'interno di un blocco syncronized
                non viene cambiata la lista di particelle, fino a che queste non saranno finite di ridisegnare
             */
            synchronized(this) {
                mParticlesToView = pos;
            }
            SwingUtilities.invokeLater(()->this.repaint());
        }
    }
}
