package view.mapper;

public class MapUtils {

    public static int mapInRange(double valueToMap, int maxRange){
        return (int) (valueToMap * maxRange + maxRange / 2);
    }

}
