package view;

import model.interfaces.Particle;

import java.awt.*;

public interface ParticleDrawer {
    void draw(Particle particle, Graphics2D g);
}
